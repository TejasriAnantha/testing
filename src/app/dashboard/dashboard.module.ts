import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import {sharedPrimengModule } from '../shared/shared-primeng.module';
export const appRoutes: Routes = [
  { path: '', component: DashboardComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes),
    sharedPrimengModule
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
