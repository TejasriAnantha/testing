import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers:[MessageService]
})
export class DashboardComponent implements OnInit {
  msg:any={
    severity:"",
    summary:"",
    detail:""
  }
  ngOnInit()  {
   
  }
  constructor(private messageService: MessageService) {
  }

 
 
}
