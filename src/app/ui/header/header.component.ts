import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../_services/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  username: string = '';
  constructor(private AuthService: AuthenticationService) { }

  ngOnInit() {
    this.username ='Test';// JSON.parse(localStorage.getItem('currentUser')).userInfo.firstName;
  }

  logout() {
    this.AuthService.logout();
    location.reload(true);
  }

}
