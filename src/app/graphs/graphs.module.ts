import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GraphsComponent } from './graphs.component';
import {sharedPrimengModule } from '../shared/shared-primeng.module';

@NgModule({
  imports: [
    CommonModule,
    sharedPrimengModule
  ],
  declarations: [GraphsComponent]
})
export class GraphsModule { }
