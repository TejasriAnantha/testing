import { NgModule } from "@angular/core";
import {CommonModule} from '@angular/common';
import {NgForm, FormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from "ngx-bootstrap";
import {DataViewModule} from 'primeng/dataview';
import {CardModule} from 'primeng/card';
import {GrowlModule} from 'primeng/growl';
import {ToggleButtonModule} from 'primeng/togglebutton';
import {PaginatorModule} from 'primeng/paginator';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {CheckboxModule} from 'primeng/checkbox';
import {TableModule} from 'primeng/table';
import {FileUploadModule} from 'primeng/fileupload';
import {DialogModule} from 'primeng/dialog';
import {DropdownModule} from 'primeng/dropdown';
import {PanelModule} from 'primeng/panel';
import {ToastModule} from 'primeng/toast';
import { TableComponent } from '../shared/table-component';
import { AppToastComponent } from '../shared/toast-component';
import {NavComponent } from '../shared/breadcrump.component';
import { BsDatepickerModule } from 'ngx-bootstrap';


@NgModule({
	declarations:[
		TableComponent,
		AppToastComponent,
		NavComponent,
		
		
		],
		providers:[],
	imports: [
			RouterModule,
			ReactiveFormsModule,
			CommonModule,
			ConfirmDialogModule,
			DataViewModule,
			DialogModule,
			TableModule,
			GrowlModule,
			FormsModule,
			PanelModule,
			CardModule,
			DropdownModule,
			ToastModule,
			ModalModule.forRoot(),
			BsDatepickerModule.forRoot()
        ],
        exports:[
			DataViewModule,
			DialogModule,
			TableModule,
			FormsModule,
			PanelModule,
            CardModule,
			GrowlModule,
			DropdownModule,
			ConfirmDialogModule,
			ReactiveFormsModule,
			ToastModule,
			TableComponent,
			AppToastComponent,
			NavComponent,
			ModalModule,
			BsDatepickerModule
        ]	
})

export class sharedPrimengModule{  }