import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-toast',
  template: `
  <p-toast [style]="{marginTop: '80px'}"></p-toast>
  `,
  providers:[MessageService]
})
export class AppToastComponent implements OnInit {
   
    
    constructor( private messageService:MessageService) { }

    ngOnInit() { 
     }
    show(severity:any,summary:any,detail:any) {
        this.messageService.add({severity:severity, summary: summary, detail:detail});
    }
  
}