import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-table',
  template: ` <app-table></app-table>`
//   <h3 class="first">Basic</h3>
//   <h3 class="first">Single Sort</h3>
//   <p-table [columns]="cols" [value]="cars">
//       <ng-template pTemplate="header" let-columns>
//           <tr>
//               <th *ngFor="let col of columns" [pSortableColumn]="col.field">
//                   {{col.header}}
//                   <p-sortIcon [field]="col.field" ariaLabel="Activate to sort" ariaLabelDesc="Activate to sort in descending order" ariaLabelAsc="Activate to sort in ascending order"></p-sortIcon>
//               </th>
//           </tr>
//       </ng-template>
//       <ng-template pTemplate="body" let-rowData let-columns="columns">
//           <tr>
//               <td *ngFor="let col of columns">
//                   {{rowData[col.field]}}
//               </td>
//           </tr>
//       </ng-template>
//   </p-table>
 
})
export class TableComponent implements OnInit {
    cars: any=[];

    cols: any[];
    @Input()  myData: any;

    constructor() { }

    ngOnInit() {

        this.cols = [
            { field: 'vin', header: 'Vin' },
            {field: 'year', header: 'Year' },
            { field: 'brand', header: 'Brand' },
            { field: 'color', header: 'Color' }
        ];
        this.cars=this.myData
    }
}