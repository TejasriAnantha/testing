import { Routes } from '@angular/router';
import { AuthComponent } from './auth.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { GraphsComponent } from '../graphs/graphs.component';
import { ReportsComponent } from '../reports/reports.component';
import { AuthGuard } from './auth.guard';

export const authRoutes: Routes = [
  {
    path: '', component: AuthComponent, children: [
      // { path: 'dashboard', loadChildren: '../dashboard/dashboard.module#DashboardModule' },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'masters', loadChildren: '../masters/masters.module#MastersModule' },
      { path: 'graphs', component: GraphsComponent },
      { path: 'reports', component: ReportsComponent },
    ]
  }
];
