import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { authRoutes } from './lazyload.routes';
import { AuthComponent } from './auth.component';

import { DashboardModule } from '../dashboard/dashboard.module';
import { UiModule } from '../ui/ui.module';
import { MastersModule } from '../masters/masters.module';
import { GraphsModule } from '../graphs/graphs.module';
import { ReportsModule } from '../reports/reports.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(authRoutes),
    UiModule,
    DashboardModule,
    MastersModule,
    GraphsModule,
    ReportsModule
  ],
  declarations: [AuthComponent]
})
export class AuthModule { }
