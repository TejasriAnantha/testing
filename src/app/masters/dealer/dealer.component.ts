import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpParams, HttpHeaders } from "@angular/common/http";
import { MasterService } from '../../../_services/master.service';
import { first } from 'rxjs/operators';
import { ConfirmationService } from 'primeng/api';
import { Message } from 'primeng/components/common/api';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { MessageService } from 'primeng/api';
import {sharedPrimengModule} from '../../shared/shared-primeng.module';
 
@Component({
  selector: 'app-dealer',
  templateUrl: './dealer.component.html',
  styleUrls: ['./dealer.component.scss'],
  providers: [ConfirmationService, MessageService]
})
export class DealerComponent implements OnInit {
  parentText: string = "Master";
  childText: string = "Dealer";
  profile={};
  dlrObj={};
  isAdd = false;
  isViewDealer=false;
  loading:boolean;
  isEdit = true;
  isDelete = true;
  stateName:any;
  date = new Date();
  emailValidator = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$/;
  pincode = /^\d{6}$/;
  numbers = /^[1-9]\d*(\.\d+)?$/;
  mobileNumber = /^[789]\d{9}$/;
  telNumber = /^[0-9]\d{2,4}-\d{6,8}$/;
  zones: any;
  // Pagination
  pageTitle: string;
  dealerList = [];
  districts = [];
  cities = [];
  errorMessage = "";
  successMessage = "";
  editItem = {};
  countries = [];
  countryName: any
  zoneName:any
  dealer:any;
  states:any;
  newItem:any;
  viewDealer:any;
  deleteSuccessmsg:any;
  @ViewChild('autoShownModal') autoShownModal: ModalDirective;
   isAddModalShown: boolean = false;
  constructor(private router: Router,
    private confirmationService: ConfirmationService,
    private formBuilder: FormBuilder,
    private services: MasterService,
     private modalService: BsModalService,
    private _reportService: MasterService,
    private _toast: MessageService) {
  }

  ngOnInit() {
    this.getDealers();
    //this.getCountries();
  }
  getDealers() {
    this.services.getDealer().subscribe(data => {
      this.dealerList = data['msg'];
      //console.log(this.dealerList)
    },
      error => {
        console.log("error");
      }
    );
  }
  getCountries() {
    this._reportService.getCountries()
      .subscribe(data => {
        this.countries = data['countries'];
        console.log(this.countries)
      }, error => {
        this._toast.add({ severity: 'danger', summary: 'Error in getting countries' });
      });
  }
  getZones(countryName) {
      this._reportService.getZones(countryName)
      .subscribe(data => {
        this.zones = data['zones'];
      }, error => {
        this._toast.add({ severity: 'danger', summary: 'Error in getting zones' });
      });

  }
  getStates(zone) {
    this.zoneName = zone.name;
    this._reportService.getStates(zone.name)
      .subscribe(data => {
        this.states = data['states'];
      }, error => {
        this._toast.add({ severity: 'danger', summary: 'Error in getting zones' });
      });
  }
  saveStateInfo(state){
    this.stateName=state.name;
  }
  openAddDealer(dealerInfo) {
    if (dealerInfo == '' || dealerInfo == "" || dealerInfo == undefined) {
      this.pageTitle = "Add New Dealer";
      this.getCountries();
      this.dealer = {
        dealerAgreementDate:new Date(),
        dealerAppointmentDate:new Date(),
        LOIDate:new Date(),
        ExpiryLOIDate:new Date(),
        dealerShipOperationDate:new Date(),
        dealerShipCloserDate:new Date(),
        address:{
          dealerAddress:"",
          countryName:"",
          stateName:"",
          cityName:"",
          pinCode:"",
          districtName:""
        },
        dealerBGs:{
          bgSurityAmount:""
        },
        dealerHead:{
          email:"",
          mobile:"",
          name:""
        }
      };
      this.isAdd = true;
    } else {
      this.isAdd = false;
      this.dealer = Object.assign({}, dealerInfo);
      this.pageTitle = "Update Dealer Information";
      console.log(this.dealer)
    }
    this.isAddModalShown = true;
  }
  hideModal(): void {
    
   this.dealer={};
    this.autoShownModal.hide();
   
  }
  
  onHidden(): void {
   this.dealer={};
    this.isAddModalShown = false;
  
  }
  saveDealers(){
    
    if(this.isAdd)
    {
      
      var obj={        
        dealerCode:this.dealer.dealerCode,
        dealerName:this.dealer.dealerName,
        dealershipType:this.dealer.dealershipType,
        address:this.dealer.address.dealerAddress,
        countryName:this.dealer.address.countryName,
        dealerZone:this.dealer.dealerZone,
        stateName:this.dealer.address.stateName,
        city:this.dealer.address.cityName,
        pincode:this.dealer.address.pinCode,
        dealerHead:this.dealer.dealerHead.name,
        dealerHeadEmail:this.dealer.dealerHead.email,
        dealerHeadMobile:this.dealer.dealerHead.mobile,
        dealerTelNum:this.dealer.dealerTelNum,
        dealerFaxNum:this.dealer.dealerFaxNum,
        dealerAgreementDate:this.dealer.dealerAgreementDate,
        dealerAppointmentDate:this.dealer.dealerAppointmentDate,
        LOIDate:this.dealer.LOIDate,
        ExpiryLOIDate:this.dealer.ExpiryLOIDate,
        dealerShipOperationDate:this.dealer.dealerShipOperationDate,
        dealerShipCloserDate:this.dealer.dealerShipCloserDate,
        dealerGST:this.dealer.dealerGST,
        dealerPAN:this.dealer.dealerPAN,
        dealerLegalEntity:this.dealer.dealerLegalEntity,
        dealerAoc:this.dealer.areaOfConcertration,
        notificationAlert:this.dealer.dealerBGs.notificationAlert,
        surityAmount:this.dealer.dealerBGs.bgSurityAmount,
        vehiclesLimit:this.dealer.dealerBGs.bgVehicleLimit
      }
      console.log(obj)
      this.newItem=this.dealer;
      this._reportService.addDealerdata(obj)
      .pipe(first())
      .subscribe(data => {
        this.dealerList.push(this.newItem);
        console.log(this.dealerList)
        this.loading = false;
        this._toast.add({severity:'success', summary:'New Dealer added successfully'});
    },error=>{
      console.log(error);
      this.loading = false;
      this._toast.add({severity:'danger', summary:'Error in Machine Add'});
        });
        this.onHidden();
    }
    }
    deleteDealer(dlrInfo){
      this.dlrObj={
        //id:dlrInfo.id,
        status:dlrInfo.dealerStatus
      }
      this._reportService.deleteDealerdata(this.dlrObj).subscribe(data => {
        this.deleteSuccessmsg = data['msg'];
        console.log(this.deleteSuccessmsg)
      }, error => {
        this._toast.add({ severity: 'danger', summary: 'Network error, please try again' });
      });
    }
    viewFullInfo(dlr){
      this.viewDealer=dlr;
      this.isViewDealer=true;
      console.log(this.viewDealer)
    }

  }
