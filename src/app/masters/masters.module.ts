import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DealerComponent} from './dealer/dealer.component';
import { Routes, RouterModule } from '@angular/router';
import {sharedPrimengModule } from '../shared/shared-primeng.module';
const masterRoutes: Routes = [
  { path: 'dealer', component: DealerComponent }
];

@NgModule({
  imports: [
    CommonModule,
    sharedPrimengModule,
    RouterModule.forChild(masterRoutes)
  ],
  declarations: [DealerComponent],
  exports: []
})
export class MastersModule { }
