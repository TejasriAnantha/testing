import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../_models/user';
import { environment } from '../environments/environment';
@Injectable({ providedIn: 'root' })
export class MasterService {
    token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NDQ1ODc4NDY5NDR9.RnHBj0-FC3HPW9fvxXKmDnH9JZAt9Me7IKBAizgt6bI";
   
    constructor(private http: HttpClient) { }
    getCountries() {
        // let headers = new HttpHeaders().set('token', this.token);
        return this.http.get(`${environment.apiUrl}/countries`);
    }
    getZones(countryName){
        let headers = new HttpHeaders().set('token', this.token); 
        return this.http.get(`${environment.apiUrl}/getZonesByCountry?countryName=${countryName}` )
    
      }
      getStates(zone){
        let headers = new HttpHeaders().set('token', this.token); // create header object
        return this.http.get(`${environment.apiUrl}/getStatesByZone?zoneCode=${zone}`)
    
      }
    
    getDealer(){
        let headers = new HttpHeaders().set('token', this.token);
        return this.http.post(`${environment.apiUrl}/dealer/dealers`,{useType:'ALL'},{headers: headers});
      }
      addDealerdata(add) {    
        let headers = new HttpHeaders().set('token', this.token);        
        return this.http.post(`${environment.apiUrl}/dealer/createDealer`, add, { headers: headers });
      }
      deleteDealerdata(dlr){
        let headers=new HttpHeaders().set('token', this.token); 
        return this.http.post(`${environment.apiUrl}/dealer/changeDealerStatus`, dlr, { headers: headers });
      }


}